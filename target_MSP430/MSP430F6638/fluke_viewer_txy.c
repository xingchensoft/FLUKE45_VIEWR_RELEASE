/*
 * fluke_viewer_txy.c
 *
 * 本驱动程序将配合FLUKE VIEWER TXY上位机使用，上位机向设备发送一个c字符
 * 本程序就会将用户绑定的变量值发送到上位机以供分析。
 * 本软件自由使用，遵守MIT开源协议，任何人可以自由修改分发本软件
 *
 *  Created on: 2015年5月11日
 *      Author: tanxiaoyao
 */

#include "fluke_viewer_txy.h"

volatile double *monitorData;		//监视的数据，bind的时候将目标数的指针传过来就可以实现实时跟踪了

/********************初始化设备管脚、晶振等***************************/
void initFVT() {
	P8SEL |= BIT2 + BIT3;
	P8OUT |= BIT2 + BIT3;					//使用P82、3的串口功能
	while (BAKCTL & LOCKBAK)                // 解锁低频晶振
		BAKCTL &= ~(LOCKBAK);
	UCSCTL6 &= ~(XT1OFF);					// 打开晶振
	UCSCTL6 |= XCAP_3;
	do {									//等待晶振起振
		UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
		SFRIFG1 &= ~OFIFG;					//清除错误标志
	} while (SFRIFG1 & OFIFG);				//校验错误标志
	UCA1CTL1 |= UCSWRST;
	UCA1CTL1 |= UCSSEL_1;                   //ACLK驱动
	UCA1BR0 = 0x03;							// 32kHz/9600=3.41 9600波特率
	UCA1BR1 = 0x00;
	UCA1MCTL = UCBRS_3 + UCBRF_0;
	UCA1CTL1 &= ~UCSWRST;
	UCA1IE |= UCRXIE;						//开启中断用以响应上位机命令
	__enable_interrupt();					//允许全局中断
}

/*****************绑定要监视的数据***************************/
void bindDataFVT(volatile double *sendData) {
	monitorData = sendData;
}

/*****************打印字符函数**************************/
void printCharFVT(unsigned char sendChar) {
	while (!(UCA1IFG & UCTXIFG))
		;             //等待发送就绪
	UCA1TXBUF = sendChar;
}

/****************打印字符串函数************************/
void printStringFVT(char *sendString) {
	UCA1IE &= ~UCRXIE;		//暂时关闭接收中断
	while ((*sendString) != '\0') {
		printCharFVT(*sendString);
		sendString++;
	}
	UCA1IE |= UCRXIE;		//重新开始接收
}

/***************接收字符串*************************/
//char *tempStr;
char buffer[32] = "";
void getStringFVT() {
	if (UCA1RXBUF != 'c') {				//判断上位机发送的请求验证符（为了加快速度及验证简便，仅用一个c字符验证）
		//*tempStr = UCA1RXBUF;
		//tempStr++;
	} else {
		sprintf(buffer, "%lf=>\n", *monitorData);	//double转string
		printStringFVT(buffer);
	}
}

//串口接收中断
#pragma vector=USCI_A1_VECTOR
__interrupt void USCI_A1_ISR(void) {
	switch (__even_in_range(UCA1IV, 4)) {
	case 0:
		break;
	case 2:
		getStringFVT();			//接收字符串
		break;
	case 4:
		break;
	default:
		break;
	}
}
